# -*- coding:utf-8 -*-
from django.contrib import admin
from blog.models import Article, Category


# class zone
# c'est ici que nous allons modifier un peu le comportement
# par défault de nos form / models dans l'admin
class AdminArticle(admin.ModelAdmin):
    # ici on défini le tri par défault
    date_hierarchy = 'created'
    # on exclu le slug car rempli en auto
    exclude = ('slug',)
    # on sélectionne les champs visibles
    list_display = ('title', 'created', 'updated')
    # on active les filtres dans l'admin
    list_filter = ('cat',)
    # on précise les champs sur lesquels nous vons chercher
    search_fields = ["title"]


class AdminCategory(admin.ModelAdmin):
    exclude = ('slug',)

# register zone
# on peux voir ici qu'on enregistre égalment notre class
# ce qui va permettre du custom l'admin pour ce model
admin.site.register(Article, AdminArticle)
admin.site.register(Category, AdminCategory)
