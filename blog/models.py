# -*- coding: utf-8 -*-
from django.db import models
from django.template.defaultfilters import slugify
from ckeditor.fields import RichTextField
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.files.images import get_image_dimensions

#standard lib import
import os
from cStringIO import StringIO

#special lib import
from PIL import Image


class Article(models.Model):

    """
    Model de gestion des articles
    title: string
    slug: string unique
    image: string représentant le chemin du fichier
    content: string / longtext pour sqlite3
        => on utilise ici RichTextField pour avoir un ckeditor dans le form
    created: datetime => autoset à la création de l'objet
    updated: datetime => autoset au save
    cat: objet category
    Gère automatiquement le resize d'image
    """
    title = models.CharField("titre", max_length=255)
    slug = models.SlugField()
    image = models.ImageField(upload_to='upload')
    content = RichTextField("contenue")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    cat = models.ForeignKey('Category')

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(self.title)

        # On vas definir ici un upload custom pour resize des images à la volés
        #sur la ligne du dessous, image correspond a votre champ image de votre model
        if not self.pk and not self.image:
            return

        # Il est important de faire la différence entre création et update
        # pour éviter de ré upload une même image
        old = None
        if self.pk:
            old = Article.objects.get(pk=self.pk)
        if not old or (old.image != self.image):
            image = Image.open(self.image)
            width, height = get_image_dimensions(self.image)
            # Si l'image n'est pas en rgb on la convertie
            if image.mode not in ('L', 'RGB'):
                image = image.convert('RGB')
            # Si l'image est déjà à la bonne taille on ne resize pas
            if width != 350 and height != 216:
                image = image.resize((350, 216), Image.ANTIALIAS)
            temp_handle = StringIO()
            # On save l'image en png
            image.save(temp_handle, 'png')
            temp_handle.seek(0)
            # On récupère l'image pour l'upload
            suf = SimpleUploadedFile(os.path.split(self.image.name)[-1], temp_handle.read(), content_type='image/png')
            # On appel la méthode save du champs image
            self.image.save(suf.name+'.png', suf, save=False)
        # On appel la méthode save du parent
        super(Article, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-created']


class Category(models.Model):
    """
    Model de gestion des catégories des articles
    name: string
    slug: string unique
    """
    name = models.CharField(max_length=100)
    slug = models.SlugField()

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)

    def __str__(self):
        return self.name
