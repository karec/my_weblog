# -*- coding:utf-8 -*-
from django.views.generic import ListView, DetailView
from blog.models import Article, Category
from django.shortcuts import get_object_or_404 as get_or_404


# la puissance même de django
# on indique un template et un model
# le reste est déjà fait
class Index(ListView):

    template_name = 'index.html'
    model = Article


class Detail(DetailView):

    template_name = 'detail.html'
    model = Article


# ici une ListView que nous allons custom
class ViewCat(ListView):

    template_name = 'index.html'
    model = Article

    # On ré écrit la méthode get_queryset
    # c'est elle qui renverra les data
    def get_queryset(self):
        category = get_or_404(Category, slug=self.kwargs['cat'])
        return Article.objects.filter(cat=category)

    def get_context_data(self, **kwargs):
        # on appel la base pour récuperer le context standard
        context = super(ViewCat, self).get_context_data(**kwargs)
        context['cat'] = Category.objects.get(slug=self.kwargs['cat'])
        return context
