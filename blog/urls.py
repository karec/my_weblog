from django.conf.urls import patterns, include, url
from blog.views import Index, Detail, ViewCat
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', Index.as_view(), name='index'),
    url(r'^article/(?P<slug>[\w-]+)/$', Detail.as_view(), name='article'),
    url(r'^categorie/(?P<cat>[\w-]+)/$', ViewCat.as_view(), name='category'),

    # uniquement pour une version de dev !!!
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
