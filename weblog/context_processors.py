# -*- coding:utf-8 -*-
from blog.models import Category


def getCats(request):
    return {"cats": Category.objects.all()}
