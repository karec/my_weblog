from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'', include('blog.urls')),
                       url(r'grappelli/', include('grappelli.urls')),
                       url(r'^admin/', include(admin.site.urls), name='admin'),
                       (r'^ckeditor/', include('ckeditor.urls')),
)
